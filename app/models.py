from google.appengine.ext import db
import xml.parsers.expat
import xml.dom.minidom
import creole
from google.appengine.api import mail
from google.appengine.api import mail_errors
from webob.exc import HTTPBadRequest, HTTPNotFound


allowed_elements = {
    'a': ['href'], 'br': [], 'p': [], 'h1': [], 'h2': [], 'h3': [],
    'h4': [], 'h5': [], 'ul': [], 'li': [], 'em': [], 'strong': [],
    'dl': [], 'dt': [], 'dd': [], 'pre': [], 'kbd': []}

tag_map = (('i', 'em'), ('tt', 'kbd'))
conv = []
for old, new in tag_map:
    conv.append(('<' + old + '>', '<' + new + '>'))
    conv.append(('</' + old + '>', '</' + new + '>'))


def creole_as_html(creole_txt):
    html = creole.creole2html(creole_txt)
    for old, new in conv:
        html = html.replace(old, new)
    return html


def check_node(node):
    if node.nodeType == node.TEXT_NODE:
        pass
    elif node.nodeType == node.ELEMENT_NODE:
        if node.tagName not in allowed_elements.keys():
            raise HTTPBadRequest(
                "The element '" + node.tagName + """' is not allowed. The
                allowed elements are """ + str(allowed_elements.keys()))
        allowed_attrs = allowed_elements[node.tagName]
        attrs = node.attributes
        for i in range(attrs.length):
            attr = attrs.item(i)
            if attr.name not in allowed_attrs:
                raise HTTPBadRequest(
                    "The only allowed attributes of the '" +
                    node.tagName + "' element are " +
                    str(allowed_attrs) + ".")
        for sub_node in node.childNodes:
            check_node(sub_node)
    else:
        node_types = {
            node.ELEMENT_NODE: 'element',
            node.ATTRIBUTE_NODE: 'attribute', node.TEXT_NODE: 'text',
            node.CDATA_SECTION_NODE: 'cdata_section',
            node.ENTITY_NODE: 'entity',
            node.PROCESSING_INSTRUCTION_NODE: 'processing_instruction',
            node.COMMENT_NODE: 'comment_node',
            node.DOCUMENT_NODE: 'document',
            node.DOCUMENT_TYPE_NODE: 'document_type',
            node.NOTATION_NODE: 'notation'}
        raise HTTPBadRequest(
            "The document can't contain a node of type " +
            node_types[node.nodeType] + ".")


def check_creole(creole_str):
    try:
        dom = xml.dom.minidom.parseString(
            '<polifesto>' + creole_as_html(creole_str).encode('utf_8') +
            '</polifesto>')
    except xml.parsers.expat.ExpatError as e:
        raise HTTPBadRequest('Problem parsing the policy: ' + e.message)
    for node in dom.documentElement.childNodes:
        check_node(node)


class Editor(db.Model):
    name = db.StringProperty(required=False)
    emails = db.StringListProperty(required=True)
    proposed_email = db.StringProperty(required=False, default='')

    @staticmethod
    def get_by_key(editor_key):
        editor = Editor.get(editor_key)
        if editor is None:
            raise HTTPNotFound(
                "There isn't an editor with key " + editor_key + ".")
        return editor


class Policy(db.Model):
    @staticmethod
    def get_by_key(key):
        try:
            policy = Policy.get(key)
            if policy is None:
                raise HTTPNotFound(
                    "There isn't a policy with key " + key)
            return policy
        except db.BadKeyError as e:
            raise HTTPNotFound(
                "There isn't a policy with key " + key + ". " + str(e))

    rating = db.IntegerProperty(required=True, default=0)
    title = db.StringProperty(required=True, default='placeholder')
    content = db.TextProperty(required=True, default='placeholder')
    tags = db.StringListProperty(default=[])

    def get_latest_version(self):
        return PolicyVersion.gql(
            "where policy = :1 order by creation_date desc limit 1",
            self).get()

    def content_as_html(self):
        return creole_as_html(self.content)

    def create_version(self, title, content, tag_str, editor):
        check_creole(content)
        tags = tag_str.split(' ')
        try:
            policy_version = PolicyVersion(
                policy=self, title=title, content=content, tags=tags,
                editor=editor)
        except db.BadValueError as e:
            if str(e) == 'Property title is required':
                raise HTTPBadRequest("The title field can't be blank.")
            else:
                raise HTTPBadRequest(str(e))
        policy_version.put()
        self.title = title
        self.content = content
        self.tags = tags
        self.put()
        editor_policy = EditorPolicy.gql(
            "where policy = :1 and editor = :2", self, editor).get()
        if editor_policy is None:
            editor_policy = EditorPolicy(editor=editor, policy=self, rating=0)
        editor_policy.email_changes = True
        editor_policy.put()

        for editor_policy in EditorPolicy.gql(
                "where policy = :1 and email_changes = TRUE", self):
            ed = editor_policy.editor
            try:
                mail.send_mail(
                    "Tony Locke <tlocke@tlocke.org.uk>", ed.name + '<' +
                    ed.emails[0] + '>',
                    "New version of '" + title, "Hi " + ed.name +
                    ", this is an automated email from Polifesto to nofify "
                    "you that a new version of the policy '" + title +
                    "' has been created. The policy can be seen at:\n\n "
                    "http://www.polifesto.com/view_policy?policy_key="
                    + str(self.key()) +
                    "\n\nYou can stop these notifications by going to the "
                    "policy and signing in and unchecking the email "
                    "notification checkbox.\n\nCheers,\n\nTony Locke of "
                    "Polifesto")
            except mail_errors.BadRequestError as e:
                if str(e) == 'Invalid recipient address':
                    pass
                else:
                    raise e
        return policy_version

    def add_comment(self, editor, comment_str):
        check_creole(comment_str)
        comment = Comment(editor=editor, policy=self, comment=comment_str)
        comment.put()

        for editor_policy in EditorPolicy.gql(
                "where policy = :1 and email_changes = TRUE", self):
            ed = editor_policy.editor
            mail.send_mail(
                "Tony Locke <tlocke@tlocke.org.uk>", ed.name + "<" +
                ed.emails[0] + '>',
                "New comment on '" + self.title + "'", "Hi " + ed.name +
                """, this is an automated email from Polifesto to nofify """
                """you that there's a new comment on the policy '""" +
                self.title + """'. The comment can be seen at:

http://www.polifesto.com/view_policy?policy_key=""" + str(self.key()) +
                """

You can stop these notifications by going to the policy and signing in """
                """and unchecking the email notification checkbox.

Cheers,

Tony Locke of Polifesto""")
        return comment


class EditorPolicy(db.Model):
    editor = db.ReferenceProperty(Editor, required=True)
    policy = db.ReferenceProperty(Policy, required=True)
    rating = db.RatingProperty(required=True)
    rating_last_modified = db.DateTimeProperty(auto_now_add=True)
    email_changes = db.BooleanProperty(required=True, default=False)


class Comment(db.Model):
    editor = db.ReferenceProperty(Editor, required=True)
    policy = db.ReferenceProperty(Policy, required=True)
    timestamp = db.DateTimeProperty(auto_now_add=True)
    comment = db.TextProperty(required=True)

    def comment_as_html(self):
        return creole_as_html(self.comment)


class PolicyVersion(db.Model):
    policy = db.ReferenceProperty(Policy, required=True)
    title = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    creation_date = db.DateTimeProperty(auto_now_add=True)
    editor = db.ReferenceProperty(Editor, required=True)
    tags = db.StringListProperty(required=True)

    def content_as_html(self):
        return creole_as_html(self.content)
