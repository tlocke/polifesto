from google.appengine.api import urlfetch, users
import jinja2
import webapp2
import difflib
from webapp2_extras import sessions
import webob.exc
import random
import string
import urllib
import markupsafe
import json
import models
from models import Comment, Policy, Editor, PolicyVersion, EditorPolicy
import datetime
from webob.exc import HTTPBadRequest
import logging

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader('templates'))

MIME_MAP = {'html': 'text/html', 'atom': 'application/atom+xml'}


def urlencode_filter(s):
    if type(s) == 'Markup':
        s = s.unescape()
    s = s.encode('utf8')
    s = urllib.quote_plus(s)
    return markupsafe.Markup(s)

jinja_environment.filters['urlencode'] = urlencode_filter


class PolifestoHandler(webapp2.RequestHandler):
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)
        self.session = self.session_store.get_session()

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        except webob.exc.HTTPUnauthorized:
            self.return_template(401, {'template_name': '401.html'})
        except webob.exc.HTTPNotFound as e:
            self.return_template(
                404, {'template_name': '404.html', 'message': str(e)})
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

    def add_flash(self, message):
        self.session.add_flash(message)

    def return_template(self, status, template_values):
        user = users.get_current_user()
        if user is None:
            template_values['login_url'] = users.create_login_url('/welcome')
        else:
            template_values['logout_url'] = users.create_logout_url('/')
            template_values['user'] = user
            editor = self.find_current_editor()
            if editor is not None:
                template_values['current_editor'] = editor
        self.response.status = status

        try:
            template_name = template_values['template_name']
        except KeyError:
            template_name = template_names[self.__class__.__name__]

        mime_type = MIME_MAP[template_name.split('.')[-1]]
        self.response.headers['Content-Type'] = mime_type
        template = jinja_environment.get_template(template_name)
        self.response.out.write(
            template.render(
                request=self.request,
                flashes=self.session.get_flashes(), **template_values))

    def return_ok(self, template_values):
        self.return_template('200 OK', template_values)

    def return_found(self, location):
        self.redirect(location, 302)

    def send_bad_request(self, template_values):
        self.return_template('400 BAD REQUEST', template_values)

    def return_unauthorized(self):
        self.abort(401)

    def return_see_other(self, location):
        self.redirect(location, code=303)

    def get_str(self, name):
        try:
            return self.request.GET[name]
        except KeyError:
            raise HTTPBadRequest("The field " + name + " is needed.")

    def post_str(self, name):
        try:
            return self.request.POST[name]
        except KeyError:
            raise HTTPBadRequest("The field " + name + " is needed.")

    def post_int(self, name):
        return int(self.post_str(name))

    def post_bool(self, name):
        return self.post_str(name) is not None

    def find_current_editor(self):
        user = users.get_current_user()
        if user is None:
            return None
        else:
            return Editor.gql("where emails = :1", user.email()).get()

    def require_current_editor(self):
        editor = self.find_current_editor()

        if editor is None:
            self.return_unauthorized()
        else:
            return editor


class SignIn(PolifestoHandler):
    def post(self):
        # The request has to have an assertion for us to verify
        if 'assert' not in self.request.POST:
            self.abort(400, detail="parameter 'assert' is required.")
        server_name = self.request.server_name
        if 'user_email' in self.request.POST and server_name == 'localhost':
            user_email = self.post_str('user_email')
            self.session['user_email'] = user_email
            return "You are logged in."
        else:
            # Send the assertion to Mozilla's verifier service.
            audience = self.request.scheme + "://" + server_name + ":" + \
                str(self.request.server_port)
            data = urllib.urlencode(
                {
                    'assertion': self.request.POST['assert'],
                    'audience': audience})
            resp = urlfetch.fetch(
                'https://verifier.login.persona.org/verify',
                payload=data,
                method=urlfetch.POST,
                headers={'Content-Type': 'application/x-www-form-urlencoded'})

            # Did the verifier respond?
            if resp.status_code == 200:
                # Parse the response
                verification_data = json.loads(resp.content)

                # Check if the assertion was valid
                if verification_data['status'] == 'okay':
                    # Log the user in by setting a secure session cookie
                    self.session['user_email'] = verification_data['email']
                    return 'You are logged in'
            logging.error("server replied " + resp.content)
            # Oops, something failed. Abort.
            self.abort(500)


class SignOut(PolifestoHandler):
    def get(self):
        if 'user_email' in self.session:
            del self.session['user_email']
        return "Logged out."


class Welcome(PolifestoHandler):
    def get(self):
        user = users.get_current_user()
        if user is None:
            self.return_ok({})
        else:
            current_editor = self.find_current_editor()
            if current_editor is None:
                fields = {}
                proposed_editors = Editor.gql(
                    "where proposed_email = :1",
                    user.email()).fetch(10)
                if len(proposed_editors) > 0:
                    fields['proposed_editors'] = proposed_editors
                self.return_ok(fields)
            else:
                if 'return_to' in self.request.GET:
                    return_to = self.get_str('return_to')
                else:
                    return_to = "/"
                self.return_found(return_to)

    def post(self):
        user = users.get_current_user()
        if user is None:
            self.return_unauthorized()

        if 'associate' in self.request.POST:
            current_editor = self.find_current_editor()
            if current_editor is None:
                editor_key = self.post_str('editor_key')
                editor = Editor.get_by_key(editor_key)
                if editor.proposed_email == user.email():
                    editor.proposed_email = ''
                    editor.emails.append(user.email())
                    editor.put()
                    self.add_flash(
                        'The email address ' + user.email() +
                        " has been successfully associated with this editor.")
                    self.return_see_other(
                        '/view_editor?editor_key=' + str(editor.key()))
                else:
                    self.return_ok(
                        messages=[
                            "Can't associate " + user.email() +
                            " with the account " + editor.name + """ because
                            the email address you're signed in with doesn't
                            match the proposed email address."""])
            else:
                self.send_bad_request(
                    messages=[
                        "The email address " + user.email() +
                        " is already associated with an account."])
        else:
            self.return_see_other('/')


class AddEditor(PolifestoHandler):
    def post(self):
        user = users.get_current_user()
        current_editor = self.find_current_editor()
        if current_editor is None:
            name = self.post_str('name')
            editor = Editor.all(keys_only=True).filter('name', name).get()
            if editor is not None:
                raise HTTPBadRequest("I'm afraid that name's already taken.")
            current_editor = Editor(emails=[user.email()], name=name)
            current_editor.put()
            self.add_flash("Account created successfully.")
            self.return_see_other('/')
        else:
            self.add_flash("You've already got an account!")
            self.return_return_see_other('/')


class ViewEditor(PolifestoHandler):
    def get(self):
        editor_key = self.get_str('editor_key')
        editor = Editor.get_by_key(editor_key)
        self.return_ok(self.make_fields(editor))

    def make_fields(self, editor):
        editor_policies = EditorPolicy.gql(
            "where editor = :1", editor).fetch(100)
        return {'editor': editor, 'editor_policies': editor_policies}


class EditEditor(PolifestoHandler):
    def get(self):
        editor = self.require_current_editor()
        self.return_ok(self.make_fields(editor))

    def make_fields(self, editor, message=None):
        editor_ratings = EditorPolicy.gql(
            "where editor = :1", editor).fetch(100)
        return {'editor': editor, 'editor_ratings': editor_ratings,
                'messages': None if message is None else [message]}

    def post(self):
        editor = self.require_current_editor()

        if 'remove_email' in self.request.POST:
            email = self.post_str('email')
            if email in editor.emails:
                editor.emails.remove(email)
                editor.put()
                self.return_ok(self.make_fields(editor,
                               "Successfully removed email."))
            else:
                self.send_bad_request(
                    self.make_fields(
                        editor, """That email isn't associated with
                        the editor."""))

        elif 'propose_email' in self.request.POST:
            proposed_email = self.post_str('proposed_email')
            editor.proposed_email = proposed_email.strip()
            editor.put()
            fields = self.make_fields(
                editor, 'Proposed email successfully set to blank.'
                if len(proposed_email) == 0 else '''Proposed email set
                successfully. Now sign out and then sign in using
                the proposed email.''')
            self.return_ok(fields)
        else:
            name = self.post_str('name')
            editor.name = name
            editor.put()
            self.add_flash('Editor updated successfully.')
            self.return_see_other(
                '/view_editor?editor_key=' + str(editor.key()))


class ViewPolicy(PolifestoHandler):
    def make_fields(self, policy, message=None):
        messages = [] if message is None else [message]
        comments = Comment.gql(
            "where policy = :1 order by timestamp desc", policy).fetch(40)
        fields = {
            'policy': policy, 'latest_version': policy.get_latest_version(),
            'messages': messages, 'comments': comments}
        editor = self.find_current_editor()
        if editor is not None:
            editor_policy = EditorPolicy.gql(
                "where editor = :1 and policy = :2", editor, policy).get()
            if editor_policy is not None:
                fields['editor_policy'] = editor_policy
        return fields

    def get(self):
        policy_key = self.get_str('policy_key')
        policy = Policy.get_by_key(policy_key)
        self.return_ok(self.make_fields(policy))

    def post(self):
        policy_key = self.post_str('policy_key')
        policy = Policy.get_by_key(policy_key)
        editor = self.require_current_editor()
        try:
            if 'rating' in self.request.POST:
                rating = self.post_int('rating')
                editor_policy = EditorPolicy.gql(
                    "where editor = :1 and policy = :2", editor, policy).get()
                if editor_policy is None:
                    editor_policy = EditorPolicy(
                        editor=editor, policy=policy, rating=rating).put()
                    policy.rating += rating
                    policy.put()
                elif editor_policy.rating != rating:
                    policy.rating = policy.rating - editor_policy.rating + \
                        rating
                    policy.put()
                    editor_policy.rating = rating
                    editor_policy.last_modified = datetime.datetime.utcnow()
                    editor_policy.put()

                self.return_ok(self.make_fields(
                    policy, 'Rating has been updated.'))
            elif 'notification' in self.request.POST:
                email_changes = self.post_bool('email_changes')
                editor_policy = EditorPolicy.gql(
                    "where editor = :1 and policy = :2", editor, policy).get()
                if editor_policy is None:
                    editor_policy = EditorPolicy(
                        editor=editor, policy=policy, rating=0,
                        email_changes=email_changes).put()
                    policy.put()
                elif editor_policy.email_changes != email_changes:
                    editor_policy.email_changes = email_changes
                    editor_policy.put()

                self.return_ok(self.make_fields(
                    policy, 'Notification preference has been updated.'))

            else:
                comment_str = self.post_str('comment')
                comment_str = comment_str.strip()
                if len(comment_str) == 0:
                    raise HTTPBadRequest("You can't add a blank comment.")
                else:
                    policy.add_comment(editor, comment_str)
                    self.add_flash("Comment added.")
                    self.return_see_other(
                        '/view_policy?policy_key=' + str(policy.key()))
        except HTTPBadRequest as e:
            self.send_bad_request(self.make_fields(
                policy, str(e)))


class ViewPolicyVersion(PolifestoHandler):

    def get(self):
        policy_version_key = self.get_str('policy_version_key')
        policy_version = PolicyVersion.get(policy_version_key)
        self.return_ok(
            {
                'policy': policy_version.policy,
                'policy_version': policy_version, 'latest_policy_version':
                policy_version.policy.get_latest_version()})


class ViewPolicyVersions(PolifestoHandler):

    def get(self):
        policy_key = self.get_str('policy_key')
        policy = Policy.get(policy_key)
        versions = PolicyVersion.gql("""where policy = :1 order by
                creation_date desc""", policy).fetch(20)
        self.return_ok(
            {
                'policy': policy, 'versions': versions,
                'latest_policy_version': policy.get_latest_version()})


class PolicyFeed(PolifestoHandler):
    def get_version_lines(self, version):
        return [version.title, ''] + version.content.splitlines() + \
            ['', ' '.join(version.tags)]

    def get(self):
        policy_key = self.get_str('policy_key')
        policy = Policy.get(policy_key)
        versions = PolicyVersion.gql(
            "where policy = :1 order by creation_date desc",
            policy).fetch(10)
        version_dicts = []
        for i, version in enumerate(versions):
            if i == len(versions) - 1:
                from_lines = []
            else:
                from_lines = self.get_version_lines(versions[i + 1])
            version_dicts.append(
                {
                    'version': version,
                    'diff': difflib.HtmlDiff().make_table(
                        from_lines, self.get_version_lines(version))})
        self.return_ok(
            {
                'policy': policy, 'version_dicts': version_dicts,
                'template_name': 'policy_feed.atom'})


class AddPolicy(PolifestoHandler):
    def get(self):
        self.return_ok({})

    def post(self):
        editor = self.require_current_editor()
        title = self.post_str('title')
        content = self.post_str('content')
        tags = self.post_str('tags')
        try:
            if 'preview' in self.request.POST:
                models.check_creole(content)
                preview = models.creole_as_html(content)
                self.return_ok({'preview': preview})
            else:
                policy = Policy(editor=editor)
                policy.put()
                try:
                    policy.create_version(title, content, tags, editor)
                except:
                    policy.delete()
                    raise
                self.add_flash("Policy successfully added.")
                self.return_see_other(
                    '/view_policy?policy_key=' + str(policy.key()))
        except HTTPBadRequest as e:
            self.send_bad_request({'messages': [str(e)]})


class AddPolicyVersion(PolifestoHandler):
    def get(self):
        policy_key = self.get_str('policy_key')
        policy = Policy.get(policy_key)
        self.return_ok(self.make_fields(policy))

    def post(self):
        policy_key = self.post_str('policy_key')
        policy = Policy.get(policy_key)
        editor = self.require_current_editor()
        title = self.post_str('title')
        content = self.post_str('content')
        tags = self.post_str('tags')

        try:
            if 'preview' in self.request.POST:
                models.check_creole(content)
                preview = models.creole_as_html(content)
                fields = self.make_fields(policy)
                fields.update({'preview': preview})
                self.return_ok(fields)
            else:
                policy.create_version(title, content, tags, editor)
                self.add_flash("New version successfully added.")
                self.return_see_other(
                    '/view_policy?policy_key=' + str(policy.key()))
        except HTTPBadRequest as e:
            self.send_bad_request(self.make_fields(policy, str(e)))

    def make_fields(self, policy, msg=None):
        return {'policy': policy, 'messages': [] if msg is None else [msg]}


class Home(PolifestoHandler):
    def make_fields(self):
        policies = Policy.gql("order by rating desc").fetch(30)
        latest_versions = PolicyVersion.gql(
            "order by creation_date desc").fetch(10)
        latest_comments = Comment.gql("order by timestamp desc").fetch(10)
        latest_ratings = EditorPolicy.gql(
            "order by rating_last_modified desc").fetch(10)
        return {'policies': policies, 'latest_versions': latest_versions,
                'latest_comments': latest_comments,
                'latest_ratings': latest_ratings}

    def get(self):
        self.return_ok(self.make_fields())


class ViewPolicies(PolifestoHandler):
    def get(self):
        if 'tag_name' in self.request.GET:
            name = self.get_str('tag_name')
            name = name.strip()
            policies = Policy.gql(
                "where tags = :1 order by rating desc", name).fetch(100)
        else:
            policies = Policy.all().fetch(100)
        self.return_ok({'policies': policies})


class ViewEditors(PolifestoHandler):
    def get(self):
        self.return_ok({'editors': Editor.all().fetch(100)})


routes = [
    (r'/', Home), (r'/xhr/sign_in', SignIn), (r'/welcome', Welcome),
    (r'/view_policies', ViewPolicies), (r'/view_policy', ViewPolicy),
    (r'/view_editors', ViewEditors), (r'/view_editor', ViewEditor),
    (r'/add_policy', AddPolicy), (r'/policy_feed', PolicyFeed),
    (r'/view_policy_versions', ViewPolicyVersions),
    (r'/view_policy_version', ViewPolicyVersion),
    (r'/add_policy_version', AddPolicyVersion),
    (r'/edit_editor', EditEditor), (r'/xhr/sign_out', SignOut),
    (r'/add_editor', AddEditor)]
template_names = dict(
    [
        (cls.__name__, rt[1:] + '.html') for rt, cls in
        routes if rt != '/'])
template_names['Home'] = 'home.html'
secret_key = ''.join(
    random.choice(string.ascii_uppercase + string.digits) for x in range(10))
config = {'webapp2_extras.sessions': {'secret_key': secret_key}}
app = webapp2.WSGIApplication(routes, debug=True, config=config)

'''
for editor in Editor.all():
    editor.openids.append(editor.claimed_id)
    editor.name = editor.claimed_id
    delattr(editor, 'claimed_id')
    editor.put()
for version in PolicyVersion.all():
    version.creation_date = version.datetime
    delattr(version, 'datetime')
    version.tag_list = version.tags.split(' ')
    delattr(version, 'tags')
    version.put()
for version in PolicyVersion.all():
    version.tags = version.tag_list
    delattr(version, 'tag_list')
    version.put()
for editor in Editor.all():
    for oid in editor.openids:
        editor.emails.append(oid)
    delattr(editor, 'openids')
    delattr(editor, 'proposed_openid')
    editor.put()
for editor_policy in EditorPolicy.all():
    editor_policy.delete()

for editor_rating in EditorRating.all():
    if hasattr(editor_rating, 'last_modified'):
        rating_lm = editor_rating.last_modified
    else:
        rating_lm = datetime.datetime(1900, 1, 1)

    editor_policy = EditorPolicy(
        editor=editor_rating.editor, policy=editor_rating.policy,
        rating=editor_rating.rating, rating_last_modified=rating_lm,
        email_changes = True)
    editor_policy.put()
'''
