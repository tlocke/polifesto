[
    {
        'port': 8080,
        'host': 'localhost',
        'name': "Look at home page",
        'path': '/',
        'status_code': 200},

    {
        'name': "Try logging in.",
        'path': '/_ah/login?email=test@example.com&'
        'action=Login&continue=http://localhost:8080/',
        'status_code': 302},

    {
        'name': "Create account",
        'path': "/add_editor",
        'method': "post",
        'data': {
            'name': "Test"},
        'status_code': 303},

    {
        'path': '/',
        'status_code': 200,
        'regexes': [
            r"Account created successfully."]},

    {
        'name': "Look at editor page",
        'path': "/view_editor?editor_key="
        "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGRWRpdG9yGICAgICAgIAKDA",
        'status_code': 200,
        'regexes':
            [r'Sign Out']},

    {
        'name': "Create a policy.",
        'path': '/add_policy',
        'method': "post",
        'tries': {},
        'data': {
            'title': "Abolish National Insurance",
            'content': "National Insurance should be merged with income "
            "tax. £",
            'tags': "uk tax"},
        'status_code': 303,
        'regexes': [
            r'/view_policy\?policy_key='
            r'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA']},

    {
        'name': "View the policy. Check link to atom feed is correct.",
        'path': '/view_policy?policy_key='
        'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
        'regexes': [
            r'/policy_feed\?policy_key='
            r'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA'],
        'status_code': 200},

    {
        'name': "Check web feed.",
        'path': '/policy_feed?policy_key='
        'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
        # Check link to policy version
        'regexes': [
            '/view_policy_version\?policy_version_key='
            'ahFkZXZ-cG9saWZlc3RvLWhyZHIaCxINUG9saWN5VmVyc2lvbhiAgICAgICACww'],
        'status_code': 200},
    {
        'name': "Try rating the policy.",
        'path': "/view_policy",
        'method': "post",
        'data': {
            "policy_key":
            "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA",
            "rating": "80"},
        'status_code': 200},
    {
        'path': '/view_policy?policy_key='
        'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
        'tries': {},
        'regexes': [
            r'<label>80\s*<input type="radio" name="rating" '
            r'value="80"\s*checked\s*/>']},

    {
        'name': "Check robots.txt.",
        'path': "/robots.txt",
        'status_code': 200},

    {
        'name': "Good error message with image in content.",
        'path': "/add_policy_version",
        'method': "post",
        'tries': {},
        'data': {
            'policy_key':
            'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
            'title': "Abolish National Insurance",
            'content': "{{an-image.png}} ",
            'tags': "uk tax"},
        'status_code': "400"},

    {
        'name': "Check accepts content with //em// in.",
        'path': "/add_policy_version",
        'method': "post",
        'data': {
            'policy_key':
            "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA",
            'title': "Abolish National Insurance",
            'content':
            "National Insurance //should// be merged with income tax. £",
            'tags': "uk tax"},
        'status_code': "303"},

    {
        'name': "Try adding a comment.",
        'path': "/view_policy",
        'method': "post",
        'data': {
            'policy_key':
            'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
            'comment': "I totally agree."},
        'status_code': "303"},
    {
        'name': "Try adding a blank comment.",
        'path': "/view_policy",
        'method': "post",
        'data': {
            'policy_key':
            'ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA',
            'comment': ""},
        'status_code': "400"},

    {
        'name': "Try adding a comment with a line break.",
        'path': "/view_policy",
        'method': "post",
        'data': {
            'policy_key':
            "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGUG9saWN5GICAgICAgIAJDA",
            'comment': "Look upon my works\\ye mighty"},
        'status_code': "303"},

    {
        'name': "Check 404 for non-existent policy.",
        'path': "/view_policy?policy_key=madeupkey",
        'status_code': "404"},

    {
        'name': "Try adding and deleting an email address.",
        'path': "/edit_editor",
        'method': "post",
        'data': {
            'proposed_email': "gladstone@example.com",
            'propose_email': "Propose"},
        'status_code': "200"},

    {
        'name': "Try some things while logged out.",
        'path': '/_ah/login?continue=http%3A//localhost%3A8080/&'
        'action=logout'},

    {
        'name': "Look at an editor",
        'path': "/view_editor?editor_key="
        "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGRWRpdG9yGICAgICAgIAKDA",
        'status_code': 200},

    {
        'name': "Log in",
        'path': '/_ah/login?email=gladstone@example.com&'
        'action=Login&continue=http://localhost:8080/',
        'status_code': 302},

    {
        'path': "/welcome",
        'method': "post",
        'data': {
            'editor_key':
            "ahFkZXZ-cG9saWZlc3RvLWhyZHITCxIGRWRpdG9yGICAgICAgIAKDA",
            'associate': "Associate"},
        'status_code': "303"}]
